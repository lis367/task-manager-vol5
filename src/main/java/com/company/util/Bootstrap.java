package com.company.util;

import com.company.commands.AbstractCommand;
import com.company.commands.HelpCommand;
import com.company.commands.projectCommands.ProjectClearCommand;
import com.company.commands.projectCommands.ProjectCreateCommand;
import com.company.commands.projectCommands.ProjectListCommand;
import com.company.commands.projectCommands.ProjectRemoveCommand;
import com.company.commands.taskCommands.TaskClearCommand;
import com.company.commands.taskCommands.TaskCreateCommand;
import com.company.commands.taskCommands.TaskListCommand;
import com.company.commands.taskCommands.TaskRemoveCommand;
import com.company.entity.Project;
import com.company.entity.Task;
import com.company.exception.CommandCorruptException;
import com.company.repository.ProjectRepository;
import com.company.repository.TaskRepository;
import com.company.service.ProjectService;
import com.company.service.TaskService;

import java.util.*;

public final class Bootstrap {

    private final Map<String, AbstractCommand> commands =
            new LinkedHashMap<>();

    public void registry(final AbstractCommand command) throws CommandCorruptException {
        final String cliCommand = command.command();
        final String cliDescription = command.description();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        commands.put(cliCommand, command);
    }

    public void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            execute(command);

        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) {
            return;}
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            return; }
        abstractCommand.execute();
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public void init() throws CommandCorruptException {

        // Созданы Проджект и Таск сервисы/репозитории

        ProjectService projectService = new ProjectService();
        TaskService taskService = new TaskService();
        Map<String, Project> projectRepositoryMap = new HashMap<>();
        Map<String, Task> taskRepositoryMap = new HashMap<>();
        TaskRepository taskRepository = new TaskRepository(taskRepositoryMap);
        ProjectRepository projectRepository = new ProjectRepository(projectRepositoryMap,taskRepository);

        projectService.setProjectRepository(projectRepository);
        projectService.setTaskRepository(taskRepository);
        taskService.setTaskRepository(taskRepository);
        taskService.setProjectService(projectService);

        // Созданы комманды проектов и загружены в лист Commands через registry

        AbstractCommand[] commandsInit = {new HelpCommand(this), new ProjectClearCommand(projectService,taskService),
                new ProjectCreateCommand(projectService), new ProjectListCommand(projectService),
                new ProjectRemoveCommand(projectService), new TaskClearCommand(taskService),new TaskCreateCommand(taskService),
                new TaskListCommand(taskService), new TaskRemoveCommand(taskService),
        };

        commands.clear();
        for (AbstractCommand commands: commandsInit){
            registry(commands);
        }


    }
}
