package com.company.commands;
import com.company.util.Bootstrap;


public abstract class AbstractCommand {
    public abstract String command();
    public abstract String description();
    public abstract void execute() throws Exception;
}

