package com.company.commands.taskCommands;

import com.company.commands.AbstractCommand;
import com.company.service.TaskService;

public final class TaskClearCommand extends AbstractCommand {

    private TaskService taskService;

    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Remove all tasks";
    }

    @Override
    public void execute() throws Exception {
        taskService.taskClear();
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    public TaskClearCommand(TaskService taskService) {
        this.taskService = taskService;
    }
}
