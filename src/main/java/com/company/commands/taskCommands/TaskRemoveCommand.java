package com.company.commands.taskCommands;

import com.company.commands.AbstractCommand;
import com.company.exception.ObjectIsNotFound;
import com.company.service.TaskService;

import java.util.Scanner;

public final class TaskRemoveCommand extends AbstractCommand {

    private TaskService taskService;

    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "Remove selected task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER ID");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        try {
            taskService.taskRemove(line);
        } catch (ObjectIsNotFound objectIsNotFound) {
            objectIsNotFound.printStackTrace();
            return;
        }
        System.out.println("TASK REMOVED");

    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    public TaskRemoveCommand(TaskService taskService) {
        this.taskService = taskService;
    }

}
