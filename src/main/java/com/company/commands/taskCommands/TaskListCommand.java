package com.company.commands.taskCommands;

import com.company.commands.AbstractCommand;
import com.company.entity.Task;
import com.company.service.TaskService;

import java.util.ArrayList;

public final class TaskListCommand extends AbstractCommand {

    private TaskService taskService;

    @Override
    public String command() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        ArrayList<Task> taskspool = taskService.taskList();
        for(int i=0;i<taskspool.size();i++){
            System.out.println("Task Name: "+taskspool.get(i).getName() + " Task ID: " + taskspool.get(i).getId() + " Project :"+taskspool.get(i).getProjectID());
            taskspool.remove(i);
        }
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    public TaskListCommand(TaskService taskService) {
        this.taskService = taskService;
    }
}
