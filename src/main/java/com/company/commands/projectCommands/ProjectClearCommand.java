package com.company.commands.projectCommands;

import com.company.commands.AbstractCommand;
import com.company.service.ProjectService;
import com.company.service.TaskService;

public final class ProjectClearCommand extends AbstractCommand {

    private ProjectService projectService;
    private TaskService taskService;

    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Remove all projects";
    }

    @Override
    public void execute() throws Exception {
        projectService.projectClear();
        taskService.taskClear();
        System.out.println("ALL PROJECT&TASK REMOVED");

    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }


    public ProjectClearCommand(ProjectService projectService, TaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

}
