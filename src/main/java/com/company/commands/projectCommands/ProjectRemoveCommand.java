package com.company.commands.projectCommands;

import com.company.commands.AbstractCommand;
import com.company.exception.ObjectIsNotFound;
import com.company.service.ProjectService;

import java.util.Scanner;

public final class ProjectRemoveCommand extends AbstractCommand {

    private ProjectService projectService;

    @Override
    public String command() {
        return "project-remove";
    }

    @Override
    public String description() {
        return "Remove selected project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER ID");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        try {
            projectService.projectRemove(line);
        } catch (ObjectIsNotFound objectIsNotFound) {
            System.out.println("ПРОЕКТ НЕ НАЙДЕН");
        }
        System.out.println("PROJECT&TASK REMOVED");

    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public ProjectRemoveCommand(ProjectService projectService) {
        this.projectService = projectService;
    }
}
