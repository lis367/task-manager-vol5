package com.company.commands.projectCommands;

import com.company.commands.AbstractCommand;
import com.company.entity.Project;
import com.company.service.ProjectService;

import java.util.ArrayList;

public final class ProjectListCommand extends AbstractCommand {

    private ProjectService projectService;


    @Override
    public String command() {
        return "project-list";
    }
    @Override
    public String description() {
        return "Show all projects.";
    }
    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        ArrayList <Project> projectpool = projectService.projectList();
        for(int i=0; i<projectpool.size();i++){
            System.out.println("Project Name: "+projectpool.get(i).getName() + "Project ID: " + projectpool.get(i).getId());
            projectpool.remove(i);
        }

    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public ProjectListCommand(ProjectService projectService) {
        this.projectService = projectService;
    }
}
