package com.company.service;

import com.company.entity.Project;
import com.company.exception.ObjectIsNotFound;
import com.company.repository.ProjectRepository;
import com.company.repository.TaskRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class ProjectService {

    private ProjectRepository projectRepository;
    private TaskRepository taskRepository;

    public String projectCreate(String name, Date dateStart, Date dateEnd) {
        String id = UUID.randomUUID().toString();
        Project project = new Project(name, id);
        project.setDateBegin(dateStart);
        project.setDateEnd(dateEnd);
        projectRepository.merge(project);
        return project.getId();
    }

    public ArrayList projectList() {
        return projectRepository.findAll();
    }

    public void projectClear() {
        projectRepository.removeAll();
    }

    public void projectRemove(String id) throws ObjectIsNotFound {
        if(projectRepository.findOne(id)==null){
            throw new ObjectIsNotFound();
         }
        else {
            projectRepository.remove(id);
            ArrayList<String>copyofTask = taskRepository.getTasksFromProject(id);
            for(int i=0;i<copyofTask.size();i++){
                taskRepository.remove(copyofTask.get(i));
            }
        }
    }

    public Project findProject(String id){
        return projectRepository.findOne(id);
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public void setProjectRepository(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }


    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

}
