package com.company.repository;

import com.company.entity.Project;
import com.company.entity.Task;
import com.company.exception.ObjectIsAlreadyExist;

import java.util.*;

public class ProjectRepository {

    public Map<String, Project> projectRepository;
    private TaskRepository taskRepository;


    public Project findOne (String id){
        return projectRepository.get(id);
    }

    public ArrayList<Project> findAll(){
        ArrayList<Project> projects = new ArrayList<>(projectRepository.values());
        return projects;
    }

    public void persist(Project project) throws Exception // При персисте, ты по ид достаёшь из мапы, если пусто сохраняешь то, что прислал сервис
    {
        Project projectPersist = projectRepository.get(project.getId());
        if (projectPersist ==null){
            projectRepository.put(project.getId(), project);
        }
        else throw new ObjectIsAlreadyExist();
    }

    public void merge (Project project) // при мерже ты просто толкаешь в мапу
    {
       projectRepository.put(project.getId(),project);
    }

    public void remove(String id){
        projectRepository.remove(id);
        ArrayList<Task> taskWithProject = taskRepository.getTasksFromProject(id);
        for (int i=0;i<taskWithProject.size();i++){
            taskRepository.remove(taskWithProject.get(i).getId());
        }

    }

    public void removeAll(){
        projectRepository.clear();
        taskRepository.removeAll();
    }

    public Project getFromProjectRepository(String name){
        return projectRepository.get(name);
    }

    public void setProjectRepositoryMap(Map<String, Project> projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public ProjectRepository(Map<String, Project> projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

}
